const express = require('express')
const jwt = require('jsonwebtoken')
let secret = require('../config/secret')
const app = express()
const path = require('path');

app.set("views", path.join(__dirname, "views"))
app.set("view engine", "pug")

module.exports = (req, res) => {
try {
   payload = jwt.verify(req.query.token, secret) //Vérifier la signature d’un token
   console.log("valid token")
} catch(err) {
   console.log(err)
}

//affichage du formulaire de saisie de mot de passe 
res.render('passwordForm')
}