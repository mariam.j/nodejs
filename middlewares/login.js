const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
let secret = require('../config/secret')


module.exports = (db) =>
async (req, res) => { 
  try{
    //console.log(`${payload.email}`)
    let user = await db('users').select().where({email: `${payload.email}`})
      if (user.length) {
        if (user[0].is_active){
          let token = jwt.sign({id: user[0].id }, secret)
          await bcrypt.compare(req.body.password, user[0].password, (err, isMatch) => {
            if (err) throw err
            else if (!isMatch) return res.send('Invalid password')
            else {
              res.cookie('token', token, { maxAge: 3600 })
              res.redirect("/home")
            }
          })
        }else{
          res.send("Your account is not activated")}
      }
     else {
      res.send('No users found.')
     }
  }
  catch(e){
     console.error(e);
  }
} 