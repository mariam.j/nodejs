const knex = require('knex')
const jwt = require('jsonwebtoken')
const nodemailer = require('nodemailer')
const config = require('../knexfile.js')
let secret = require('../config/secret')
const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const mailService = require('../services/mailService')
const mail = require('../mail/index.js')
const { sendMail } = require('../mail/index.js')
const app = express(feathers())
let db = knex(config.development)


module.exports =async(req, res) => {
    try {     
      await db.into('users').insert(req.body)
    }
    catch(err) {
      return res.status(403).json({error: 'User already exists'})
    }

    // create a token
    let token = jwt.sign({email:req.body.email}, secret, {algorithm: 'HS256', expiresIn: 86400})// expires in 24 hours

    // send a confirmation mail
    const confirmationUrl = 'http://localhost:3000/confirm-registration?token=' + token

  async function main() {
      let user = req.body
      try{ 
       const result = await sendMail({
         from: 'mariam.enseeiht@gmail.com',
         to: user.email,
         subject: 'Email Confirmation',
         content: `Click this link below to verify your email adress: ${confirmationUrl}`
      })
    console.log(result)}   
      catch(e){
      console.error(e);
    }}
   main()
}