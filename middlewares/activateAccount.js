const bcrypt = require('bcrypt')
const knex = require('knex')
let config = require('../knexfile.js')
let db = knex(config.development)

module.exports = async (req, res) => {

    try {
        let saltRounds = 10
        let hash = bcrypt.hashSync(req.body.password, saltRounds)
        await db('users').update({is_active: 1, password : hash}).where({email: req.body.email})
        console.log('active')
        res.redirect('signin')
}
catch (err) {
    console.log (err)
  }
}
