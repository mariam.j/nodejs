const path = require('path');
const knex = require('knex');
const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const mailService = require('./services/mailService.js')
const config = require('./knexfile.js');
let db = knex(config.development);

// Create a feathers instance.
const app = express(feathers())

app.set("views", path.join(__dirname, "views"))
app.set("view engine", "pug")

app.use(express.static(path.join(__dirname, "public")))

app.use(express.urlencoded({extended: true}))
app.use(express.json())

//Transport: HTTP 
app.configure(express.rest())

// Custom services
app.configure(mailService)


//middlewares
const createAccount = require('./middlewares/create-account')
const confirmRegistration = require ('./middlewares/confirmRegistration')
const activateAccount = require ('./middlewares/activateAccount')
const login = require ('./middlewares/login')


app.get("/", (req, res) => {
   res.render("index", {
      url1: "http://localhost:3000/signup",
      url2: "http://localhost:3000/signin"
   })
})

app.get('/signup', (request, response) => {
   response.render('signup')
})

app.get('/signin', (request, response) => {  
   response.render('signin');
  }) 

  app.get('/home', (request, response)=>{
   response.render('home')
})

app.post('/create-account', createAccount)
app.get('/confirm-registration', confirmRegistration)
app.post('/activate-account', activateAccount)
app.post('/login', login(db))

//let userService = service({Model: db, name: 'users'})
app.use('/users', service({
   Model: db,
   name: 'users'
 }));
app.listen(3000, function () {
  console.log("Server listening on port 3000")
})

