const knex = require('knex')
const config = require('../knexfile.js')

async function main() {
   try {
      let db = knex(config.development)
      let usersTableExists = await db.schema.hasTable('users')
      if (usersTableExists) {
         console.log('table "users" already exists')
      } else {
         exports.up = await db.schema.createTable('users', table => {
            table.increments('id')
            table.string('email').unique().notNull()
            table.string('password').notNull()
            table.string('fullname')
            
            table.boolean('is_active').defaultTo(false)
         })
         console.log('table "users" created')
      }
      process.exit(0)
    } catch(err) {
       console.log(err.toString())
    }
}
main()

